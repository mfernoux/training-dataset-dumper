#include "JetTruthAssociator.hh"

#include "TruthTools.hh"

#include "xAODBTagging/BTaggingUtilities.h"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "MCTruthClassifier/MCTruthClassifierDefs.h"

namespace {
  template <typename T> using Acc = SG::AuxElement::ConstAccessor<T>;

  bool stable_not_geant(const xAOD::TruthParticle& x) {
    if (x.status() != 1) return false;      // not stable
    if (x.barcode() > 200000) return false; // geant
    return true;
  }

  std::function<bool(const xAOD::TruthParticle&)> get_selector(
    TruthSelectorConfig::Particle particle)
  {
    using p = TruthSelectorConfig::Particle;
    using TP = xAOD::TruthParticle;
    namespace MC = MCTruthPartClassifier;
    Acc<unsigned int> type_acc("classifierParticleType");
    Acc<unsigned int> orig_acc("classifierParticleOrigin");
    switch (particle) {
    case p::hadron:
      return [](const TP& x){
        return truth::isWeaklyDecayingHadron(x, 5) || truth::isWeaklyDecayingHadron(x, 4);
      };
    case p::lepton:
      return [](const TP& x){
        return truth::isFinalStateChargedLepton(x);
      };
    case p::fromBC:
      return [](const TP& x) -> bool {
        if (x.status() != 1) return false;
        return truth::getParentHadron(&x);
      };
    case p::overlapLepton:
      return [orig_acc](const TP& x) {
        if (!x.isElectron() and !x.isMuon()) return false;
        if (x.status() != 1) return false;
        unsigned int o = orig_acc(x);
        if (o == MC::WBoson or o == MC::ZBoson or o == MC::top) return true;
        return false;
      };
    case p::promptLepton:
      return [type_acc](const TP& x) {
        if (!stable_not_geant(x)) return false;
        unsigned int t = type_acc(x);
        if (t == MC::IsoElectron or t == MC::IsoMuon) return true;
        return false;
      };
    case p::nonPromptLepton:
      return [type_acc](const TP& x) {
        if (!stable_not_geant(x)) return false;
        unsigned int t = type_acc(x);
        if (t == MC::NonIsoElectron or t == MC::NonIsoMuon) return true;
        return false;
      };
    case p::muon:
      return [type_acc](const TP& x) {
        if (!stable_not_geant(x)) return false;
        unsigned int t = type_acc(x);
        if (t == MC::IsoMuon or t == MC::NonIsoMuon) return true;
        return false;
      };
    case p::stableNonGeant:
      return [](const TP& x) { return stable_not_geant(x); };
    default:
      throw std::logic_error("unknown particle type");
    }
  }
}

// the constructor just builds the decorator
JetTruthAssociator::JetTruthAssociator(const std::string& link_name,
                                       TruthSelectorConfig config):
  m_deco(link_name),
  m_kinematics(config.kinematics),
  m_selector(get_selector(config.particle))
{
  containers = config.containers;
}

// decorate truth particles passing selection
void JetTruthAssociator::decorate(const xAOD::Jet& jet,
                                  TruthContainers* tpcs) const
{  
  PartLinks links;
  for (const auto* tpc : *tpcs) {
    for ( const auto* part : *tpc ) {
      if ( not passed_cuts(*part, jet) ) {
        continue;
      }
      PartLink link(*tpc, part->index());
      links.push_back(link);
    }
  }
  m_deco(jet) = links;
}

// selections
bool JetTruthAssociator::passed_cuts(const xAOD::TruthParticle& part,
                                    const xAOD::Jet& jet) const
{
  if ( part.pt() < m_kinematics.pt_minimum ) return false;
  if ( std::abs(part.eta()) > m_kinematics.abs_eta_maximum ) return false;
  if (jet.p4().DeltaR(part.p4()) > m_kinematics.dr_maximum ) return false;
  return m_selector(part);
}
