# setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

echo "=== running setupATLAS ==="
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

echo "=== running asetup ==="
# Some of the newly added code only works in recent nightlies, see ATLINFR-4697
# This is to be moved to a numbered release once it is available
asetup Athena,master,r2022-09-22T2101

# add h5ls
SCRIPT_PATH=${BASH_SOURCE[0]:-${0}}
if [[ $SCRIPT_PATH =~ / ]] ; then
    source ${SCRIPT_PATH%/*}/add-h5-tools.sh
else
    source setup/add-h5-tools.sh
fi

# make the job fail on flake8 warnings.
export FLAKE8_ATLAS_ERROR=1
